import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        printBonusDatesBetween(2010, 2010);
        printBonusDatesBetween(2010, 2015);
        printBonusDatesBetween(1900, 2100);
        printBonusDatesBetween(3000, 3100);
    }

    // Prints out palindrome days. Format yyyy-mm-dd
    public static void printBonusDatesBetween(int fromYear, int toYear) {
        if (fromYear > toYear) {
            System.out.println("Year format is incorrect");
            return;
        }

        Calendar mainCalendar = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();
        
        cal.set(Calendar.YEAR, fromYear);
        cal.set(Calendar.DAY_OF_YEAR, 1);
        
        Date startDate = cal.getTime();

        cal.set(Calendar.YEAR, toYear);
        cal.set(Calendar.DAY_OF_YEAR, 1);

        Date endDate = cal.getTime();

        System.out.printf("Bonus dates between %tF and %tF:\n", startDate, endDate);

        mainCalendar.setTime(startDate);
        int palindromeDays=0;
        
        while (mainCalendar.getTime().before(endDate)) {
            int year = mainCalendar.get(Calendar.YEAR);
            int month = mainCalendar.get(Calendar.MONTH) + 1;
            int day = mainCalendar.get(Calendar.DAY_OF_MONTH);

            int monthAndDay = month * 100 + day;
            int reversedYear = 0;

            while (year != 0) {
                int digit = year % 10;
                reversedYear = reversedYear * 10 + digit;
                year /= 10;
            }

            if (reversedYear == monthAndDay) {
                System.out.printf("%tF\n", mainCalendar.getTime());
                palindromeDays++;
            }

            mainCalendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        System.out.printf("Total of %d palindrome days found.\n\n", palindromeDays);
    }
}
